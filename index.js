require("colors");
const fs = require("fs");
const path = require("path");
const _ = require("lodash");
const { prompt } = require("inquirer");

const WRITE_PATH = path.resolve("output");
const SPLIT_STRING = `
`;

// const CHUNK_NUM = 3;

const walk = function(dir) {
  let results = [];
  const list = fs.readdirSync(dir);
  list.forEach(function(file) {
    file = dir + "/" + file;
    const stat = fs.statSync(file);
    if (stat && stat.isDirectory()) results = results.concat(walk(file));
    else results.push(file);
  });
  return results;
};
const required = function(value) {
  return !!value;
};

prompt({
  type: "input",
  name: "CHUNK_NUM",
  message: "你需要将数据拆分为多少份？",
  default: 3,
  validate: required
}).then(({ CHUNK_NUM }) => {
  const files = walk(path.resolve("input")).filter(filepath =>
    /\.txt$/.test(filepath)
  );

  console.log(`查询到${files.length}个文件需要分析`.green);

  const contents = files.map(filepath => fs.readFileSync(filepath, "UTF8"));

  /**
   * 解析文件内容
   * @param {*} content
   */
  const analysisFile = content =>
    content
      .trim()
      .split(SPLIT_STRING)
      .filter(eachContent => !_.isEmpty(eachContent));

  const sumData = contents.reduce(
    (prev, next) => prev.concat(analysisFile(next)),
    []
  );
  console.log("获取到的数据为", sumData);
  const shuffleData = _.shuffle(sumData);

  const everyNum = Math.ceil(shuffleData.length / CHUNK_NUM);

  const newFileData = _.chunk(shuffleData, everyNum);

  console.log("打乱后需要分配的数据", newFileData);

  if (!fs.existsSync(WRITE_PATH)) {
    fs.mkdirSync(WRITE_PATH);
  }

  newFileData.forEach((newContent, index) =>
    fs.writeFileSync(
      path.resolve(WRITE_PATH, `output_${index + 1}.txt`),
      newContent.join(SPLIT_STRING)
    )
  );

  console.log(
    `共${shuffleData.length}项，拆分为${CHUNK_NUM}分，每份${everyNum}项`.green
  );
  console.log("done! ".green);
});
